import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:search_filter_bloc_sample/bloc/event/user_event.dart';
import 'package:search_filter_bloc_sample/bloc/state/user_state.dart';
import 'package:search_filter_bloc_sample/model/user.dart';
import 'package:search_filter_bloc_sample/service/api_service.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  final APIService _apiService = APIService();
  UserBloc() : super(UserInit()) {
    on<GetUserList>((event, emit) async {
      emit(UserLoading());
      try {
        final response = await _apiService.getListUser();
        if (response.statusCode == 200) {
          print('res success');
          emit( UserSuccess(user: User.fromJson(jsonDecode(response.body))));
        } else {
            print('res bad');
          emit(UserError(
              user: User(
                  page: 0,
                  perPage: 0,
                  total: 0,
                  totalPages: 0,
                  data: [],
                  support: Support())));
        }
      } catch (e) {
         print('res error');
        emit(UserError(
              user: User(
                  page: 0,
                  perPage: 0,
                  total: 0,
                  totalPages: 0,
                  data: null,
                  support: Support())));
      }
    });
  }
}
