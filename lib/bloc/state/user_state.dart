import 'package:equatable/equatable.dart';
import 'package:search_filter_bloc_sample/model/user.dart';

abstract class UserState extends Equatable {
  @override
  List<Object?> get props => [];
}

class UserInit extends UserState {
  
}

class UserLoading extends UserState {
  
}

class UserSuccess extends UserState {
  final User user;
  UserSuccess({required this.user});
}

class UserError extends UserState {
  final User user;
  UserError({required this.user});
}
