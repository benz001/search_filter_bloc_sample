import 'package:http/http.dart' as http;

class APIService {
  Future<http.Response> getListUser() async {
    final response = await http.get(Uri.parse('https://reqres.in/api/users'));

    return response;
  }
}
