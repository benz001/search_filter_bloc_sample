import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:search_filter_bloc_sample/bloc/event/user_event.dart';
import 'package:search_filter_bloc_sample/bloc/handle_api/user_bloc.dart';
import 'package:search_filter_bloc_sample/bloc/state/user_state.dart';
import 'package:search_filter_bloc_sample/model/user.dart';

class UserPage extends StatefulWidget {
  const UserPage({Key? key}) : super(key: key);

  @override
  State<UserPage> createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  final UserBloc _userBloc = UserBloc();
  TextEditingController keyword = TextEditingController(text: '');
  List<DataUser> _listDataUser = [];
  List<DataUser> _listResult = [];

  @override
  void initState() {
    super.initState();
    _userBloc.add(GetUserList());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('User List'),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.grey[300],
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: 50,
              color: Colors.transparent,
              child: TextField(
                controller: keyword,
                decoration:
                    const InputDecoration(hintText: 'Enter Your Keyword'),
                onChanged: (v) {
                  print('keyword: $v');
                   _listResult = _listDataUser
                      .where((element) => element.firstName!
                          .toLowerCase()
                          .contains(v.toLowerCase()))
                      .toList();
                  _userBloc.add(GetUserList());
                },
              ),
            ),
            Expanded(
              child: BlocConsumer<UserBloc, UserState>(
                  bloc: _userBloc,
                  listener: (context, state) {
                    print('state is $state');
                    if (state is UserSuccess) {
                      if (keyword.text.isEmpty) {
                        _listDataUser = state.user.data ?? [];
                      } else {
                        _listDataUser = _listResult;
                      }
                    }
                  },
                  builder: (context, state) {
                    if (state is UserLoading) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    } else if (state is UserSuccess) {
                      return _buildListUser(context, _listDataUser);
                    } else if (state is UserError) {
                      return Container(
                        width: 100,
                        height: 50,
                        alignment: Alignment.center,
                        color: Colors.transparent,
                        child: ElevatedButton(
                            onPressed: () {
                              _userBloc.add(GetUserList());
                            },
                            child: const Text('Reload')),
                      );
                    } else {
                      return Container();
                    }
                  }),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListUser(BuildContext context, List<DataUser> listDataUser) {
    return ListView.builder(
        itemCount: _listDataUser.length,
        padding: const EdgeInsets.symmetric(horizontal: 10),
        itemBuilder: (context, index) {
          String? firstName = listDataUser[index].firstName;
          String? lastname = listDataUser[index].lastName;
          String? email = listDataUser[index].email;
          return Container(
            color: Colors.white,
            width: double.infinity,
            padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
            margin: const EdgeInsets.only(top: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Name: $firstName $lastname'),
                const SizedBox(
                  height: 5,
                ),
                Text('Email: $email')
              ],
            ),
          );
        });
  }
}
